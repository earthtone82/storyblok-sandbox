import Vue from 'vue'
import StoryblokVue from 'storyblok-vue'
import App from './App.vue'
import router from './router'
import md from './directives/md'

Vue.use(StoryblokVue)
Vue.directive('md', md)

new Vue({
  router,
  el: '#root',
  render (h) {
    return h(App)
  }
})
