import Router from 'vue-router'
import Vue from 'vue'

import Home from './views/home.vue'
import Blog from './views/blog.vue'
import About from './views/about.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/about', name: 'about', component: About },
    { path: '/blog/:slug*', name: 'blog', component: Blog }
  ]
})
