import MarkdownIt from 'markdown-it'

export default {
  inserted (el, binding) {
    const md = new MarkdownIt()
    el.innerHTML = md.render(binding.value)       
  }
}
